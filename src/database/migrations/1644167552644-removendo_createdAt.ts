import {MigrationInterface, QueryRunner} from "typeorm";

export class removendoCreatedAt1644167552644 implements MigrationInterface {
    name = 'removendoCreatedAt1644167552644'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "users" DROP COLUMN "createdAt"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "users" ADD "createdAt" TIMESTAMP NOT NULL`);
    }

}
