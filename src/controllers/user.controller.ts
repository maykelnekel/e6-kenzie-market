import { NextFunction, Request, Response } from "express"
import { getRepository } from "typeorm"
import { User } from "../entities"


export const list = async (req: Request, res: Response, next: NextFunction) => {
    const userRepository = getRepository(User)

    const users = await userRepository.find()

    res.send(users)
    
}

export const create = async (req: Request, res: Response, next: NextFunction) => {
    const userRepository = getRepository(User)
    const data = req.body;
    try{
        const find = await userRepository.findOne({ where: { email: data.email } });
        if (find) {
            res.status(400).send({ message: "E-mail already registered" });
        } else {
        const user = userRepository.create({ ...data });
        await userRepository.save(user);
        res.status(201).send(user);

        }
    } catch (err) {
        console.log(err);
    }    
}

export const getUser = async (req: Request, res: Response, next: NextFunction) => {
    const userRepository = getRepository(User)

    const {id} = req.params

    const user = await userRepository.findOne({id: id})

    res.send(user)

}