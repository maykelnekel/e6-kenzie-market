import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import  User from "./users.entity";

@Entity('addresses')
class Addres{
    @PrimaryGeneratedColumn('uuid')
    id!: string

    @Column()
    zipCode!: string

    @Column()
    address!: string

    @Column()
    city!: string
    
    @OneToMany(type => User, adresses => Addres)
    user!: User

}

export default Addres