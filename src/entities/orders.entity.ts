import { Column, Entity, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import User  from "./users.entity";
import Product from "./products.entity";

@Entity('orders')
class Order{
    @PrimaryGeneratedColumn('uuid')
    id!: string

    @Column({type: 'timestamp'})
    date!: Date

    @ManyToMany(type=> Product, orders=> Order)
    @JoinTable()
    products!: Product

    @OneToMany(type => User, orders=> Order)
    user!: User

}

export default Order