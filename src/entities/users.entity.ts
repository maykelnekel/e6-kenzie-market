import {Entity, PrimaryGeneratedColumn, Column, ManyToOne} from "typeorm";
import Order from "./orders.entity";
import Addres from "./adresses.entity";

@Entity('users')
class User {

    @PrimaryGeneratedColumn('uuid')
    id!: string;

    // @Column({type: 'timestamp'})
    // createdAt!: Date;

    @Column()
    firstName!: string;

    @Column()
    lastName!: string;

    @Column()
    password!: string;

    @Column()
    phone!: string;

    @Column()
    isAdm!: boolean;

    @Column()
    email!: string;

    @ManyToOne(type => Addres, user => User, {eager: true})
    adress!: Addres;

    @ManyToOne(type=> Order, user=> User)
    orders!: Order;


}

export default User