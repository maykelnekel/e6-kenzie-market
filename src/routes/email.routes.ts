import { Express } from "express"
import express from 'express'

const router = express.Router()

const emailRoutes = (app: Express) => {
    router.get('', (req, res) => res.send({message: "rodando"}))

    app.use('/email', router)
}

export default emailRoutes