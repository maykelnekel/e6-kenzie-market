import { Express } from "express"
import express from 'express'

const router = express.Router()

const recuperarRoutes = (app: Express) => {
    router.get('', (req, res) => res.send({message: "rodando"}))

    app.use('/recuperar', router)
}

export default recuperarRoutes