import { Express } from "express"
import express from 'express'

const router = express.Router()

const alterarSenhaRoutes = (app: Express) => {
    router.get('', (req, res) => res.send({message: "rodando"}))

    app.use('/alterar_senha', router)
}

export default alterarSenhaRoutes