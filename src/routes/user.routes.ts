import { Express } from "express"
import express from 'express'
import { list, create, getUser } from "../controllers/user.controller"

const router = express.Router()

const userRoutes = (app: Express) => {
    router.get('', list)
    router.post('', create)
    router.get('/:id', getUser)

    app.use('/user', router)
}

export default userRoutes