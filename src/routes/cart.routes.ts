import { Express } from "express"
import express from 'express'

const router = express.Router()

const cartRoutes = (app: Express) => {
    router.get('', (req, res) => res.send({message: "rodando"}))

    app.use('/cart', router)
}

export default cartRoutes