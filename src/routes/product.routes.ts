import { Express } from "express"
import express from 'express'

const router = express.Router()

const productRoutes = (app: Express) => {
    router.get('', (req, res) => res.send({message: "rodando"}))

    app.use('/product', router)
}

export default productRoutes